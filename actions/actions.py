# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


import os
import requests

from typing import Any, Text, Dict, List, Tuple
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet


class ActionGetWeather(Action):

    def name(self) -> Text:
        return "get_weather"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        location = tracker.get_slot("location")

        if location is None:
            return [SlotSet("location", None)]
        
        coords = self.geocode_forward(location)
        weather = self.get_weather(coords)

        dispatcher.utter_message(text=weather)
        return [ SlotSet("location", location)]
    
    def get_weather(self, coords: Tuple[float, float]) -> Text:
        api_key = os.environ['PIRATE_WEATHER_API_KEY']
        lat, lng = coords

        url = f"https://api.pirateweather.net/forecast/{api_key}/{lat},{lng}"
        params = {
            "exclude": "minutely,hourly,daily,alerts,flags",
            "units": "si"
        }

        r = requests.get(url, params=params)

        data = r.json()

        precipitation = data["currently"]["precipIntensity"]
        temperature = data["currently"]["temperature"]

        return f"The weather is {temperature}°C with a precipitation of {precipitation}mm/h"
    
    def geocode_forward(self, location: Text) -> Tuple[float, float]:
        url = "https://api.opencagedata.com/geocode/v1/json"
        params = {
            "key": os.environ["OPEN_CAGE_API_KEY"],
            "q": location,
        }
        
        r = requests.get(url, params=params)
        data = r.json()

        geometry = data["results"][0]["geometry"]

        lat, lng = geometry["lat"], geometry["lng"]

        return lat, lng
