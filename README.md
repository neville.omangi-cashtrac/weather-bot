# Rasa weather

## Requirements

- ~~Detect intent check weather~~
- ~~Detect entity location~~
  - Confirm location
- ~~Use action to return actual weather in location~~
- Use form to ask for location + time + weather element

## Getting started

```sh
# git init
# touch .gitignore

rtx local python 3.10.18
python -m venv venv
source venv/bin/activate

pip install --upgrade pip
pip install -d black flake8 pytype pytest pytest-asyncio pytest-sanic
pip install typing-extensions wheel rasa[spacy] requests

rasa init --no-prompt

env $(< .env xargs -d '\n') rasa run actions
rasa train
rasa shell
```
